
# This is a benchmark program for the phobos digests

It can be compiled with all D compilers (at the moment of writting).

It output the benchmark results.

Use
```dub run --compiler=ldc2 -b release```
or
```dub run --compiler=dmd -b release```
or
```dub run --compiler=gdc -b release```

Use configurations to build the different variants of the code:
* local-dev - a local copy of the xxh.d source (development aide)
* dub-package - use the xxhash3 DUB package
* phobos-dev - Use std.digest.xxh . You must use the beta DMD compiler in this case

# Outputs

```bash
$ dub run --compiler=ldc2 -b release -c dub-package -- -h
Performing "release-debug" build using ldc2 for x86_64.
xxhash3 0.0.3: building configuration "library"...
hashbench 0.0.2+commit.1.g8ad2e1a: building configuration "dub-package"...
Running pre-build commands...
Linking...
Running hashbench -h
Some information about the program.
-v      --verbose Verbose outputs (WIP)
-t         --test Select a test out of [all, crc32, crc64ecma, crc64iso, xxh32, xxh64, xxh3_64, xxh3_128, murmur32, murmur128, md5]
   --max-duration Set time limit for each test (seconds)
-w     --writecsv Write CSV files
-h         --help This help information.

$ dub run --compiler=ldc2 -b release -c dub-package -- 
Performing "release" build using ldc2 for x86_64.
xxhash3 0.0.3: building configuration "library"...
hashbench 0.0.2+commit.1.g8ad2e1a: building configuration "dub-package"...
Running pre-build commands...
Linking...
Running hashbench 
A benchmark utility for the phobos digests.

Build is v0.0.2-1-g8ad2e1a

Time Limit per test is 180
              test_crc32.test_crc32: Average  2819.148193 MB/s
      test_crc64ecma.test_crc64ecma: Average  2118.051270 MB/s
        test_crc64iso.test_crc64iso: Average  2121.853271 MB/s
              test_xxh32.test_xxh32: Average  6808.921387 MB/s
              test_xxh64.test_xxh64: Average 13470.754883 MB/s
          test_xxh3_64.test_xxh3_64: Average  9649.270508 MB/s
        test_xxh3_128.test_xxh3_128: Average  9634.948242 MB/s
            test_murmur32.test_mm32: Average  3273.416992 MB/s
          test_murmur128.test_mm128: Average  6258.288574 MB/s
                  test_md5.test_md5: Average   726.315247 MB/s
Total test duration: 9 secs, 263 ms, 665 μs, and 5 hnsecs
```
