/** Module containing general helper routines and templates to minimize dupblicate code */
module test_pattern_generator;

import std.stdint;
import std.format;

/* Test Pattern Generator */

/** PatternGenerator Modes */
enum PatterMode
{
    Unknown, /** Unknown default init value */
    Uniform, /** Uniform fill with same byte, maximum redundancy */
    RepeatN, /** Repeating 0..255 data, near maximum redundancy */
    PRandom, /** Pseudo random byte stream from XorShift18 and same seed, no redundancy */
    Sparse1, /** Sparsed data, mix of random sequence with
                     inserted [0..ArgN] byte value strings every ArgM bytes, 
                     only one string pattern within random data */
    SparseN /** Sparsed data, mix of random sequence with
                     inserted random value strings every ArgM bytes, 
                     multiple string pattern within random data */
}

/** Structure with test pattern mode and parameters */
struct TestPattern {
    PatterMode mode;
    uint N;
    int M, O;

    string toString() const
    {
        return format("(%s:%05x:%05x:%02x)", mode, N, M, O);
    }
}

unittest
{
    const auto tp = TestPattern().toString;
    assert ( tp == "(Unknown:00000:00000:00)" );
    const auto tp1 = TestPattern(PatterMode.Uniform, 255, 0, 0 ).toString;
    assert ( tp1 == "(Uniform:000ff:00000:00)" );
    const auto tp2 = TestPattern(PatterMode.PRandom, 32, 0, 0 ).toString;
    assert ( tp2 == "(PRandom:00020:00000:00)" );
    const auto tp3 = TestPattern(PatterMode.Sparse1, 1024, 2048, 2 ).toString;
    assert ( tp3 == "(Sparse1:00400:00800:02)" );
}

auto testPatternSet = [
    TestPattern( PatterMode.Uniform,       0,       0, 0 ), // Uniform fill with zero, maximum redundancy 
    TestPattern( PatterMode.Uniform,    0x55,       0, 0 ), // Uniform fill with 0x55, maximum redundancy 
    TestPattern( PatterMode.Uniform,    0xff,       0, 0 ), // Uniform fill with 0x55, maximum redundancy 
    TestPattern( PatterMode.RepeatN,      32,       0, 0 ), // Repeating fill from [0..32] , high redundancy
    TestPattern( PatterMode.RepeatN,     128,       0, 0 ), // Repeating fill from [0..128], high redundancy
    TestPattern( PatterMode.RepeatN,     256,       0, 0 ), // Repeating fill from [0..256], high redundancy
    TestPattern( PatterMode.Sparse1, 0x01000, 0x02000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x02000, 0x04000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x04000, 0x08000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x08000, 0x10000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x10000, 0x20000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x20000, 0x40000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x30000, 0x60000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.Sparse1, 0x40000, 0x80000, 0 ), // 50% of data pure random, 50% is of some repeating pattern
    TestPattern( PatterMode.SparseN,  0x0400,  0x0800, 8 ), // 50% of data pure random, 50% are of 8 different patterns
    TestPattern( PatterMode.SparseN,  0x0800,  0x1000, 8 ), // 50% of data pure random, 50% are of 8 different patterns
    TestPattern( PatterMode.SparseN,  0x1000,  0x2000, 8 ), // 50% of data pure random, 50% are of 8 different patterns
    TestPattern( PatterMode.SparseN,  0x2000,  0x4000, 8 ), // 50% of data pure random, 50% are of 8 different patterns
    TestPattern( PatterMode.SparseN,  0x3000,  0x6000, 8 ), // 50% of data pure random, 50% are of 8 different patterns
    TestPattern( PatterMode.SparseN,  0x4000,  0x8000, 8 ), // 50% of data pure random, 50% are of 8 different patterns
    TestPattern( PatterMode.PRandom,      32,       0, 0 ), // 5 Bit random data, 50% use of token, minimal redundancy  
    TestPattern( PatterMode.PRandom,     128,       0, 0 ), // 7 Bit random data, 50% use of token, minimal redundancy
    TestPattern( PatterMode.PRandom,     256,       0, 0 )  // Full random data, no redudancy
];


/** Fill the input buffer with a test pattern 
 *
 * Param:  input uint8_t[] Dynamic Array to fill with pattern
 * Param:  mode PatterMode Fill mode to use
 * Param:  OptN int Argument 1 for fill pattern mode
 * Param:  OptN int Argument 2 for fill pattern mode
 * Throws: assert on unknown modes or parametera
 */
void patternGenerator(uint8_t[] input, PatterMode mode, uint OptN = 255, int OptM = 0x10000, int OptO = 8)
{
    import std.random : Xorshift;

    bool rc = true;
    switch (mode)
    {
    case PatterMode.Uniform:
        assert(OptN <= 255, "OptN must be 0..255 range");
        uint8_t fillbyte = cast(uint8_t)(OptN & 0xff);
        input[] = fillbyte; // Fill complete array ;-)
        break;
    case PatterMode.RepeatN:
        assert(OptN <= 256, "OptN must be 0..256 range");
        foreach (idx; 0 .. input.length)
            input[idx] = cast(uint8_t)((idx % OptN) & 0xff);
        break;
    case PatterMode.PRandom:
        assert(OptN <= 256, "OptN must be 0..256 range");
        auto rnd = Xorshift(1); // Always the same number sequence
        foreach (idx; 0 .. input.length)
        {
            input[idx] = cast(uint8_t)((rnd.front % OptN) & 0xff);
            rnd.popFront;
        }
        break;
    case PatterMode.SparseN:
        assert(OptO > 0, "OptO must be > 0");
        goto case;
    case PatterMode.Sparse1:
        assert(OptN <= OptM, "OptN must be <= OptM");
        auto rndBack = Xorshift(4711);
        int rndInsertSeed = 1;
        for (int idx = 0; idx < input.length;)
        {
            // Write fulllength randomnumber sequence 
            input[idx++] = cast(uint8_t)(rndBack.front & 0xff);
            rndBack.popFront;

            // Insert repeating random sequences here
            const bool inspos = (OptN > 0) && ((idx % OptM) >= 0) && ((idx % OptM) < OptN);
            if (inspos)
            {
                auto rndInsert = Xorshift(rndInsertSeed);
                for (int idx2 = 0; idx < input.length && idx2 < OptN; idx2++)
                {
                    rndBack.popFront;
                    /* Insert something else here */
                    input[idx++] = cast(uint8_t)(rndInsert.front & 0xff);
                    rndInsert.popFront;
                }
                /* Use alternating patterns */
                if (mode == PatterMode.SparseN)
                    rndInsertSeed = (rndInsertSeed++ % OptO) + 1;
            }
        }
        break;
    default:
        rc = false;
        break;
    }
    assert(rc == true, "Internal error, unknown PatternMode");
}

/* Test uniform pattern */
unittest
{
    uint8_t[] data;
    data.length = 8;
    uint8_t[] comp;
    comp.length = 8;
    patternGenerator(data, PatterMode.Uniform, 0);
    comp[] = 0;
    assert(data == comp, "Fill 0x00 failed");
    patternGenerator(data, PatterMode.Uniform, 0x55);
    comp[] = 0x55;
    assert(data == comp, "Fill 0x55 failed");
    patternGenerator(data, PatterMode.Uniform, 0xaa);
    comp[] = 0xAA;
    assert(data == comp, "Fill 0xAA failed");
    patternGenerator(data, PatterMode.Uniform, 0xff);
    comp[] = 0xFF;
    assert(data == comp, "Fill 0xFF failed");
}

/* Test Repeat pattern */
unittest
{
    uint8_t[] data;
    data.length = 16;
    uint8_t[] comp;
    comp.length = 16;
    patternGenerator(data, PatterMode.RepeatN, 3);
    comp[] = [0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0];
    assert(data == comp, "Repeat modulo3 failed");
    patternGenerator(data, PatterMode.RepeatN, 4);
    comp[] = [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3];
    assert(data == comp, "Repeat modulo4 failed");
    patternGenerator(data, PatterMode.RepeatN, 8);
    comp[] = [0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7];
    assert(data == comp, "Repeat modulo4 failed");
}

/* Test PRandom pattern */
unittest
{
    uint8_t[] data;
    data.length = 16;
    uint8_t[] comp;
    comp.length = 16;
    patternGenerator(data, PatterMode.PRandom, 256, 0);
    comp[] = [
        23, 182, 139, 174, 11, 151, 22, 60, 220, 35, 99, 148, 144, 150, 42, 125
    ];
    assert(data == comp, "PRandom (256,0) failed");
    patternGenerator(data, PatterMode.PRandom, 256, 0);
    assert(data == comp, "Repeated PRandom (256,0) failed");

    comp.length = data.length = 32;
    patternGenerator(data, PatterMode.PRandom, 256, 0);
    comp[] = [
        23, 182, 139, 174, 11, 151, 22, 60, 220, 35, 99, 148, 144, 150, 42, 125,
        91, 5, 152, 51, 71, 64, 38, 160, 251, 77, 83, 32, 203, 113, 63, 119
    ];
    assert(data == comp, "PRandom (256,0) failed");
    patternGenerator(data, PatterMode.PRandom, 256, 0);
    assert(data == comp, "Repeated PRandom (256,0) failed");

    comp.length = data.length = 16;
    patternGenerator(data, PatterMode.PRandom, 32, 0);
    comp[] = [23, 22, 11, 14, 11, 23, 22, 28, 28, 3, 3, 20, 16, 22, 10, 29];
    assert(data == comp, "PRandom (32,0) failed");
    patternGenerator(data, PatterMode.PRandom, 32, 0);
    assert(data == comp, "Repeated PRandom (32,0) failed");

    comp.length = data.length = 32;
    patternGenerator(data, PatterMode.PRandom, 32, 0);
    comp[] = [
        23, 22, 11, 14, 11, 23, 22, 28, 28, 3, 3, 20, 16, 22, 10, 29, 27, 5, 24,
        19, 7, 0, 6, 0, 27, 13, 19, 0, 11, 17, 31, 23
    ];
    assert(data == comp, "PRandom (32,0) failed");
    patternGenerator(data, PatterMode.PRandom, 32, 0);
    assert(data == comp, "Repeated PRandom (32,0) failed");
}

/* Test Sparse1 pattern */
unittest
{
    uint8_t[] data;
    data.length = 16;
    uint8_t[] comp;
    comp.length = 16;

    patternGenerator(data, PatterMode.Sparse1, 0, 8);
    comp[] = [
        72, 19, 198, 31, 24, 146, 131, 103, 250, 62, 193, 79, 113, 24, 85, 117
    ];
    assert(data == comp, "Spare1 (0,8) failed");
    patternGenerator(data, PatterMode.Sparse1, 0, 8);
    assert(data == comp, "Spare1 (0,8) failed");

    patternGenerator(data, PatterMode.Sparse1, 1, 8);
    comp[] = [
        72, 19, 198, 31, 24, 146, 131, 103, 23, 62, 193, 79, 113, 24, 85, 117
    ];
    assert(data == comp, "Spare1 (1,8) failed");
    patternGenerator(data, PatterMode.Sparse1, 1, 8);
    assert(data == comp, "Spare1 (1,8) failed");

    patternGenerator(data, PatterMode.Sparse1, 2, 8);
    comp[] = [
        72, 23, 182, 31, 24, 146, 131, 103, 23, 182, 193, 79, 113, 24, 85, 117
    ];
    assert(data == comp, "Spare1 (2,8) failed");
    patternGenerator(data, PatterMode.Sparse1, 2, 8);
    assert(data == comp, "Spare1 (2,8) failed");

    patternGenerator(data, PatterMode.Sparse1, 3, 8);
    comp[] = [
        72, 23, 182, 139, 24, 146, 131, 103, 23, 182, 139, 79, 113, 24, 85, 117
    ];
    assert(data == comp, "Spare1 (3,8) failed");
    patternGenerator(data, PatterMode.Sparse1, 3, 8);
    assert(data == comp, "Spare1 (3,8) failed");

    patternGenerator(data, PatterMode.Sparse1, 7, 8);
    comp[] = [
        72, 23, 182, 139, 174, 11, 151, 22, 250, 23, 182, 139, 174, 11, 151, 22
    ];
    assert(data == comp, "Spare1 (7,8) failed");
    patternGenerator(data, PatterMode.Sparse1, 7, 8);
    assert(data == comp, "Spare1 (7,8) failed");
}

/* Test SparseN pattern */
unittest
{
    uint8_t[] data;
    data.length = 16;
    uint8_t[] comp;
    comp.length = 16;

    patternGenerator(data, PatterMode.SparseN, 0, 4, 2);
    comp[] = [
        72, 19, 198, 31, 24, 146, 131, 103, 250, 62, 193, 79, 113, 24, 85, 117
    ];
    assert(data == comp, "SpareN (0,4,2) failed");
    patternGenerator(data, PatterMode.SparseN, 0, 4, 2);
    assert(data == comp, "SpareN (0,4,2) failed");

    patternGenerator(data, PatterMode.SparseN, 2, 8, 2);
    comp[] = [
        72, 23, 182, 31, 24, 146, 131, 103, 31, 27, 193, 79, 113, 24, 85, 117
    ];
    assert(data == comp, "SpareN (2,8,2) failed");
    patternGenerator(data, PatterMode.SparseN, 2, 8, 2);
    assert(data == comp, "SpareN (2,8,2) failed");

}
