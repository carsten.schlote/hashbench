module test_helpers;

import std.stdint;
import std.datetime;

/** Shorthand for 2^^20 bytes */
enum mega = 1024 * 1024;

/** Get duration as a float in seconds
 * Param: duration
 * Returns: duration as float, unit seconds
 */
float getFloatSecond(Duration duration)
{
    const auto ns_duration = duration.total!"nsecs";
    const float second_duration = float(ns_duration) / 10 ^^ 9;
    return second_duration;
}

unittest
{
    import std.math.operations;

    auto s = getFloatSecond(dur!"seconds"(1));
    assert(isClose(s, 1.0));
    s = getFloatSecond(dur!"msecs"(500));
    assert(isClose(s, 0.5));
    s = getFloatSecond(dur!"usecs"(500));
    assert(isClose(s, 0.0005));
}

/** Calculate MB/s from bytesize and a Duration value
 * Param: byteSize - size_t Size of Data processed
 * Param: duration - Duration of processing
 * Returns:
 *   float Megabyte per second
 */
float getMegaBytePerSeconds(size_t byteSize, Duration duration)
{
    enum mega = 2 ^^ 20;
    const float second_duration = getFloatSecond(duration);
    const float megaBytesPerSec = (byteSize != 0) ?
        (float(byteSize) / mega) / float(second_duration) :
        float.nan;
    return megaBytesPerSec;
}

unittest
{
    import std.math.operations;

    auto mbs = getMegaBytePerSeconds(150 * 2 ^^ 20, dur!"seconds"(1));
    assert(isClose(mbs, 150.0));
    mbs = getMegaBytePerSeconds(10 * 2 ^^ 20, dur!"msecs"(500));
    assert(isClose(mbs, 20.0));
    mbs = getMegaBytePerSeconds(10 * 2 ^^ 20, dur!"usecs"(500));
    assert(isClose(mbs, 20_000.0));
}

/** Return compression ratio from unpacked/packed sizes
 * Param: unpacked_size - size_t Unpacked length
 * Param: packed_size - size_t Packed length
 * Returns:
 *   float with ratio. Values > 1.0 denote increased size after compression
 */
float getCompressionRatio(size_t unpacked_size, size_t packed_size)
{
    return packed_size > 0 ? float(packed_size) / float(unpacked_size) : float.nan;
}

unittest
{
    import std.math.operations;
    float ratio;
    ratio = getCompressionRatio( 1000, 1000);
    assert( isClose( ratio, 1.0f ));
    ratio = getCompressionRatio( 1000, 500);
    assert( isClose( ratio, 0.5f ));
    ratio = getCompressionRatio( 1000, 2000);
    assert( isClose( ratio, 2.0f ));
    ratio = getCompressionRatio( 0, 0);
    import std.math;
    assert( isNaN( ratio ));
}