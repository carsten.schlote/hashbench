module test_xxh3_128;

/** MD5 Checksum Routines Unittest (D module) */
import core.stdc.stdlib;
import std.datetime.stopwatch;
import std.digest;
version (UseXXHSourceFromDub) import xxhash3;
version (UseXXHSourceFromLocalDir) import local_dev.xxh;
version (UseXXHSourceFromPhobos) import std.digest.xxh;
import std.file;
import std.format;
import std.range;
import std.stdint;
import std.stdio;

import test_helpers;
import test_csv;

/** Test and benchmark the XXH3_128 functions */

int test_xxh3_128(int argTimeLimit, bool writeCSV) {
	auto csvFileName = "./docs/benchmark_xxh3_128.csv";
	auto csvFile = new TestCSVFile!XXH3_128TestData(csvFileName);

	auto timeLimitSW = StopWatch(AutoStart.yes);
	auto sw = StopWatch(AutoStart.no);
	const int rc = 0;
	float mbs_average = 0.0;
	int mbs_counts = 0;

	foreach (int size; 4 .. 16) {
		stdout.flush(); // Flush contents of stdout to avoid half done outputs in CI Pipeline
		if (timeLimitSW.peek.total!"seconds" > argTimeLimit) {
			writeln("Time Limit execeeded, Abort test.");
			break;
		}
		foreach (ubyte i; 0 .. (256 / 32)) {
			int tsize = 2 ^^ 20 * size;
			auto test = new ubyte[tsize];
			test[] = cast(ubyte)(i * 32);

			/* Test 1 */
			XXH128_canonical_t hash1;
			{
				XXH3_128 xxh;
				sw.reset;
				sw.start;
				xxh.start(); // Create a state
				for (int j = 0; j < tsize; j += tsize / 8) {
					xxh.put(test[j .. j + tsize / 8]); // Hash the file in chunks
				}
				hash1 = xxh.finish(); // Finalize the hash
				sw.stop;
			}
			auto time1 = sw.peek;

			/* Test 2 */
			XXH128_canonical_t hash3;
			{
				auto xxh = new XXH3_128Digest();
				sw.reset;
				sw.start;
				hash3 = xxh.digest(test); //&test[0], tsize, 0xbaad5eed);
				sw.stop;
			}
			auto time2 = sw.peek;
			mbs_average += getMegaBytePerSeconds(tsize, time2);
			mbs_counts++;

			/* Status output */
			version (unittest) {
			} else {
				import app : argVerbose;

				if (argVerbose)
					writefln("Input: size=0x%08x fill=0x%02x, hash %s, %8fs, %8fs, %10.6f MB/s, %10.6f MB/s",
						tsize, i, hash1.toHexString,
						getFloatSecond(time1), getFloatSecond(time2),
						getMegaBytePerSeconds(tsize, time1), getMegaBytePerSeconds(tsize, time2));
			}
			/* Prepare CSV output */
			auto testCsv = XXH3_128TestData(tsize, i, hash3, time1, time2);
			csvFile.addEntry(testCsv);

			/* Check hash results */
			assert(hash1 == hash3, "Hashes mismatch");
		}
	}
	timeLimitSW.stop;

	writefln("%35.35s: Average %12.6f MB/s", __FUNCTION__, mbs_average / mbs_counts);
	if (writeCSV)
		csvFile.writeFile;
	return rc;
}
