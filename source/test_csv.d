/** Helper to read and write CSV files */
module test_csv;

import std.datetime;
import std.digest;
import std.digest.md;
version (UseXXHSourceFromDub) import xxhash3;
version (UseXXHSourceFromLocalDir) import local_dev.xxh;
version (UseXXHSourceFromPhobos) import std.digest.xxh;
import std.digest.murmurhash;
import std.file;
import std.format;
import std.range : join;
import std.stdio;
import std.string;
import std.traits : FieldNameTuple;

import test_helpers;
import test_pattern_generator;

/** Container struct for compression benchmarking in CSV file */
struct CompressionTestData {
public:
    TestPattern pattern;
    size_t unpacked_size;
    size_t packed_size;
    Duration pack_duration;
    Duration unpack_duration;
private:
    float pack_ratio;
    float pack_mbs;
    float unpack_mbs;

    void updateHiddenFields() {
        pack_ratio = getCompressionRatio(unpacked_size, packed_size);
        pack_mbs = getMegaBytePerSeconds(unpacked_size, pack_duration);
        unpack_mbs = getMegaBytePerSeconds(unpacked_size, unpack_duration);
    }

public:
    /** Get the names of the CSV fields from the structure */
    static string getCsvHeader() {
        return [FieldNameTuple!CompressionTestData].join(';');
    }

    /** Output the structure data as a CSV Line */
    string getCsvLine() {
        updateHiddenFields();
        auto rc = format("%s;%d;%d;%f;%f;%f;%f;%f",
            pattern, unpacked_size, packed_size,
            getFloatSecond(pack_duration), getFloatSecond(unpack_duration),
            pack_ratio, pack_mbs, unpack_mbs
        );
        return rc;
    }
}

/** Check empty values and zero devision handling */
unittest {
    CompressionTestData tv;
    const auto hdr = tv.getCsvHeader;
    assert(
        hdr == "pattern;unpacked_size;packed_size;pack_duration;unpack_duration;pack_ratio;pack_mbs;unpack_mbs");
    const auto line = tv.getCsvLine;
    assert(line == "(Unknown:00000:00000:00);0;0;0.000000;0.000000;nan;nan;nan");
}

/** Check example values*/
unittest {
    auto tp = TestPattern(PatterMode.RepeatN, 0x00100, 0x00200, 8);

    CompressionTestData tv = CompressionTestData(tp, 2 ^^ 20, 2 ^^ 10, dur!"seconds"(1), dur!"seconds"(
            1));
    auto line = tv.getCsvLine;
    assert(
        line == "(RepeatN:00100:00200:08);1048576;1024;1.000000;1.000000;0.000977;1.000000;1.000000");

    tv = CompressionTestData(tp, 10 ^^ 6, 10 ^^ 3, dur!"seconds"(1), dur!"seconds"(1));
    line = tv.getCsvLine;
    assert(
        line == "(RepeatN:00100:00200:08);1000000;1000;1.000000;1.000000;0.001000;0.953674;0.953674");
}

/** Container struct for hashing benchmarking in CSV file */
struct HashTestData(HASHTYPE) {
public:
    size_t size;
    size_t fill;
    HASHTYPE hash;
    Duration duration1;
    Duration duration2;
private:
    float mbs1;
    float mbs2;

    void updateHiddenFields() {
        mbs1 = getMegaBytePerSeconds(size, duration1);
        mbs2 = getMegaBytePerSeconds(size, duration2);
    }

public:
    /** Get the names of the CSV fields from the structure */
    static string getCsvHeader() {
        return [FieldNameTuple!MD5TestData].join(';');
    }

    /** Output the structure data as a CSV Line */
    string getCsvLine() {
        import std.digest : toHexString;

        updateHiddenFields();
        static if (is(HASHTYPE == MurmurHash3!32)) {
            auto rc = format("0x%04x;0x%02x;%s;%f;%f;%f;%f", size, fill, hash.getBytes.toHexString,
                getFloatSecond(duration1), getFloatSecond(duration2),
                mbs2, mbs2);
        } else static if (is(HASHTYPE == MurmurHash3!128)) {
            auto rc = format("0x%04x;0x%02x;%s;%f;%f;%f;%f", size, fill, hash.getBytes.toHexString,
                getFloatSecond(duration1), getFloatSecond(duration2),
                mbs2, mbs2);
        } else {
            auto rc = format("0x%04x;0x%02x;%s;%f;%f;%f;%f", size, fill, hash.toHexString,
                getFloatSecond(duration1), getFloatSecond(duration2),
                mbs2, mbs2);
        }
        return rc;
    }
}

alias CRC32TestData = HashTestData!(ubyte[4]);
alias CRC64TestData = HashTestData!(ubyte[8]);
alias MD5TestData = HashTestData!(ubyte[16]);
alias XXH32TestData = HashTestData!XXH32_canonical_t;
alias XXH64TestData = HashTestData!XXH64_canonical_t;
alias XXH3_64TestData = HashTestData!XXH64_canonical_t;
alias XXH3_128TestData = HashTestData!XXH128_canonical_t;
alias Murmur32TestData = HashTestData!(ubyte[4]);
alias Murmur128TestData = HashTestData!(ubyte[16]);

/** Check empty values and zero devision handling */
unittest {
    MD5TestData tv;
    const auto hdr = tv.getCsvHeader;
    assert(hdr == "size;fill;hash;duration1;duration2;mbs1;mbs2");
    const auto line = tv.getCsvLine;
    assert(line == "0x0000;0x00;00000000000000000000000000000000;0.000000;0.000000;nan;nan");
}

/** Check example values*/
unittest {
    MD5TestData tv =
        MD5TestData(
            0x1000, 0xAA, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            dur!"seconds"(1), dur!"seconds"(1));
    auto line = tv.getCsvLine;
    assert(
        line == "0x1000;0xaa;000102030405060708090A0B0C0D0E0F;1.000000;1.000000;0.003906;0.003906");

    tv = MD5TestData(
        0x2000, 0x55, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        dur!"seconds"(1), dur!"seconds"(1));
    line = tv.getCsvLine;
    assert(
        line == "0x2000;0x55;000102030405060708090A0B0C0D0E0F;1.000000;1.000000;0.007812;0.007812");
}

/** Check empty values and zero devision handling */
unittest {
    XXH32TestData tv;
    const auto hdr = tv.getCsvHeader;
    assert(hdr == "size;fill;hash;duration1;duration2;mbs1;mbs2");
    const auto line = tv.getCsvLine;
    assert(line == "0x0000;0x00;00000000;0.000000;0.000000;nan;nan");
}

/** Check example values*/
unittest {
    XXH32TestData tv =
        XXH32TestData(
            0x1000, 0xAA, [ 12,34,56,78 ],
            dur!"seconds"(1), dur!"seconds"(1));
    auto line = tv.getCsvLine;
    assert(line == "0x1000;0xaa;0C22384E;1.000000;1.000000;0.003906;0.003906");

    tv = XXH32TestData(
        0x2000, 0x55, [ 12,34,56,78 ],
        dur!"seconds"(1), dur!"seconds"(1));
    line = tv.getCsvLine;
    assert(line == "0x2000;0x55;0C22384E;1.000000;1.000000;0.007812;0.007812");
}

/** Check empty values and zero devision handling */
unittest {
    XXH64TestData tv;
    const auto hdr = tv.getCsvHeader;
    assert(hdr == "size;fill;hash;duration1;duration2;mbs1;mbs2");
    const auto line = tv.getCsvLine;
    assert(line == "0x0000;0x00;0000000000000000;0.000000;0.000000;nan;nan");
}

/** Check example values*/
unittest {
    XXH64TestData tv =
        XXH64TestData(
            0x1000, 0xAA, [ 1,2,3,4,5,6,7,8 ],
            dur!"seconds"(1), dur!"seconds"(1));
    auto line = tv.getCsvLine;
    assert(line == "0x1000;0xaa;0102030405060708;1.000000;1.000000;0.003906;0.003906");

    tv = XXH64TestData(
        0x2000, 0x55, [ 1,2,3,4,5,6,7,8 ],
        dur!"seconds"(1), dur!"seconds"(1));
    line = tv.getCsvLine;
    assert(line == "0x2000;0x55;0102030405060708;1.000000;1.000000;0.007812;0.007812");
}

/** Simple class to collect CSV data and write it to disk */
class TestCSVFile(T) {
private:
    T[] testEntries; /// The collected test entries
    string fileName; /// Name of CSV

public:
    /** Constructors */
    this(string filename) {
        fileName = filename;
    }

    /** Add a test entry */
    void addEntry(ref T entry) {
        testEntries ~= entry;
    }

    /** Write data as a CSV file */
    void writeFile() {
        /* Convert data to CSV strings */
        string[] csvlines;
        csvlines ~= T.getCsvHeader;
        foreach (entry; testEntries)
            csvlines ~= entry.getCsvLine;
        auto csvblob = csvlines.join("\n") ~ "\n\n";
        /* And finally write them to disk */
        std.file.write(fileName, csvblob);
    }
}

unittest {
    auto fn = deleteme();
    auto tcf = new TestCSVFile!CompressionTestData(fn);
    scope (exit)
        remove(fn);
    auto tc = CompressionTestData();
    tcf.addEntry(tc);
    tcf.writeFile;

    enum expectedContents =
        "pattern;unpacked_size;packed_size;pack_duration;unpack_duration;pack_ratio;pack_mbs;unpack_mbs\n" ~
        "(Unknown:00000:00000:00);0;0;0.000000;0.000000;nan;nan;nan\n\n";
    const auto csvcontents = readText(fn);
    assert(csvcontents == expectedContents, "Mismatched file contents");
}

unittest {
    auto tp = TestPattern(PatterMode.RepeatN, 0x00100, 0x00200, 8);

    auto fn = deleteme();
    auto tcf = new TestCSVFile!CompressionTestData(fn);
    scope (exit)
        remove(fn);
    auto tc1 = CompressionTestData(tp, 2 ^^ 20, 2 ^^ 10, dur!"seconds"(1), dur!"seconds"(1));
    tcf.addEntry(tc1);
    auto tc2 = CompressionTestData(tp, 10 ^^ 6, 10 ^^ 3, dur!"seconds"(1), dur!"seconds"(1));
    tcf.addEntry(tc2);
    tcf.writeFile;

    enum expectedContents =
        "pattern;unpacked_size;packed_size;pack_duration;unpack_duration;pack_ratio;pack_mbs;unpack_mbs\n" ~
        "(RepeatN:00100:00200:08);1048576;1024;1.000000;1.000000;0.000977;1.000000;1.000000\n" ~
        "(RepeatN:00100:00200:08);1000000;1000;1.000000;1.000000;0.001000;0.953674;0.953674\n" ~
        "\n";
    const auto csvcontents = readText(fn);
    assert(csvcontents == expectedContents, "Mismatched file contents");
}

unittest {
    auto fn = deleteme();
    auto tcf = new TestCSVFile!MD5TestData(fn);
    scope (exit)
        remove(fn);
    auto tc = MD5TestData();
    tcf.addEntry(tc);
    tcf.writeFile;

    enum expectedContents =
        "size;fill;hash;duration1;duration2;mbs1;mbs2\n" ~
        "0x0000;0x00;00000000000000000000000000000000;0.000000;0.000000;nan;nan\n\n";
    const auto csvcontents = readText(fn);
    assert(csvcontents == expectedContents, "Mismatched file contents");
}

unittest {
    auto fn = deleteme();
    auto tcf = new TestCSVFile!MD5TestData(fn);
    scope (exit)
        remove(fn);
    auto tc1 = MD5TestData(
        0x1000, 0xAA, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        dur!"seconds"(1), dur!"seconds"(1));
    tcf.addEntry(tc1);
    auto tc2 = MD5TestData(
        0x2000, 0x55, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        dur!"seconds"(1), dur!"seconds"(1));
    tcf.addEntry(tc2);
    tcf.writeFile;

    enum expectedContents =
        "size;fill;hash;duration1;duration2;mbs1;mbs2\n" ~
        "0x1000;0xaa;000102030405060708090A0B0C0D0E0F;1.000000;1.000000;0.003906;0.003906\n" ~
        "0x2000;0x55;000102030405060708090A0B0C0D0E0F;1.000000;1.000000;0.007812;0.007812\n" ~
        "\n";
    const auto csvcontents = readText(fn);
    assert(csvcontents == expectedContents, "Mismatched file contents");
}
