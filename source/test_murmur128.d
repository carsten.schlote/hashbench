module test_murmur128;

/** MD5 Checksum Routines Unittest (D module) */
import core.stdc.stdlib;
import std.datetime.stopwatch;
import std.digest;
import std.digest.murmurhash;
import std.file;
import std.format;
import std.range;
import std.stdint;
import std.stdio;

import test_helpers;
import test_csv;

/** Test and benchmark the mm3232 functions */

int test_mm128(int argTimeLimit, bool writeCSV) {
	auto csvFileName = "./docs/benchmark_mm128.csv";
	auto csvFile = new TestCSVFile!Murmur128TestData(csvFileName);

	auto timeLimitSW = StopWatch(AutoStart.yes);
	auto sw = StopWatch(AutoStart.no);
	const int rc = 0;
	float mbs_average = 0.0;
	int mbs_counts = 0;

	foreach (int size; 4 .. 16) {
		stdout.flush(); // Flush contents of stdout to avoid half done outputs in CI Pipeline
		if (timeLimitSW.peek.total!"seconds" > argTimeLimit) {
			writeln("Time Limit execeeded, Abort test.");
			break;
		}
		foreach (ubyte i; 0 .. (256 / 32)) {
			int tsize = 2 ^^ 20 * size;
			auto test = new ubyte[tsize];
			test[] = cast(ubyte)(i * 32);

			/* Test 1 */
			ubyte[16] hash1;
			{
				MurmurHash3!128 mm128;
				sw.reset;
				sw.start;
				mm128.start(); // Create a state
				for (int j = 0; j < tsize; j += tsize / 8) {
					mm128.put(test[j .. j + tsize / 8]); // Hash the file in chunks
				}
				hash1 = mm128.finish(); // Finalize the hash
				sw.stop;
			}
			auto time1 = sw.peek;

			/* Test 2 */
			ubyte[16] hash3;
			{
				hash3 = digest!(MurmurHash3!128)(test);
			}
			auto time2 = sw.peek;
			mbs_average += getMegaBytePerSeconds(tsize, time2);
			mbs_counts++;

			/* Status output */
			version (unittest) {
			} else {
				import app : argVerbose;

				if (argVerbose)
					writefln("Input: size=0x%08x fill=0x%02x, hash %s, %8fs, %8fs, %10.6f MB/s, %10.6f MB/s",
						tsize, i, hash1.toHexString,
						getFloatSecond(time1), getFloatSecond(time2),
						getMegaBytePerSeconds(tsize, time1), getMegaBytePerSeconds(tsize, time2));
			}
			/* Prepare CSV output */
			auto testCsv = Murmur128TestData(tsize, i, hash3, time1, time2);
			csvFile.addEntry(testCsv);

			/* Check hash results */
			assert(hash1 == hash3, "Hashes mismatch");
		}
	}
	timeLimitSW.stop;

	writefln("%35.35s: Average %12.6f MB/s", __FUNCTION__, mbs_average / mbs_counts);
	if (writeCSV)
		csvFile.writeFile;
	return rc;
}
