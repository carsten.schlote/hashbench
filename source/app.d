module app;

import std.conv;
import std.datetime.stopwatch;
import std.format;
import std.getopt;
import std.stdio;
import std.traits;

static string _gitversion = import("git_describe.txt");

bool argVerbose = false;

/** Combined test and benchmark runner for phobos digests.
 * Param:
 *   args - string array with commandline args
 */
int main(string[] args)
{
	enum TestsEnum { all, crc32, crc64ecma, crc64iso, xxh32, xxh64, xxh3_64, xxh3_128, murmur32, murmur128, md5 }
	bool writeCSV = false;
	int argTimeLimit = 3 * 60;

	TestsEnum tests = TestsEnum.all;
	try {
		auto helpInformation = getopt(
			args,
			"verbose|v", "Verbose outputs (WIP)", &argVerbose,
			"test|t", "Select a test out of " ~ format("%s", [EnumMembers!TestsEnum]), &tests,
			"max-duration", "Set time limit for each test (seconds)", &argTimeLimit,
			"writecsv|w", "Write CSV files", &writeCSV);

		if (helpInformation.helpWanted)
		{
			defaultGetoptPrinter("Some information about the program.",
				helpInformation.options);
			return 0;
		}
	} catch (std.getopt.GetOptException goe)
	{
		writeln(goe.msg);
		return 1;
	} catch (std.conv.ConvException ce)
	{
		writeln(ce.msg);
		return 1;
	}
	int rc = 0;
	auto sw = StopWatch(AutoStart.no);
	sw.reset;
	sw.start;
	writeln("A benchmark utility for the phobos digests.\n");
	writeln("Build is ", _gitversion);
	writeln("Time Limit per test is ", argTimeLimit);
	if (tests == TestsEnum.all || tests == TestsEnum.crc32)
	{
		import test_crc32 : test_crc32;

		rc += test_crc32(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.crc64ecma)
	{
		import test_crc64ecma : test_crc64ecma;

		rc += test_crc64ecma(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.crc64iso)
	{
		import test_crc64iso : test_crc64iso;

		rc += test_crc64iso(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.xxh32)
	{
		import test_xxh32 : test_xxh32;

		rc += test_xxh32(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.xxh64)
	{
		import test_xxh64 : test_xxh64;

		rc += test_xxh64(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.xxh3_64)
	{
		import test_xxh3_64 : test_xxh3_64;

		rc += test_xxh3_64(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.xxh3_128)
	{
		import test_xxh3_128 : test_xxh3_128;

		rc += test_xxh3_128(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.murmur32)
	{
		import test_murmur32 : test_mm32;

		rc += test_mm32(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.murmur128)
	{
		import test_murmur128 : test_mm128;

		rc += test_mm128(argTimeLimit, writeCSV);
	}
	if (tests == TestsEnum.all || tests == TestsEnum.md5)
	{
		import test_md5 : test_md5;

		rc += test_md5(argTimeLimit, writeCSV);
	}
	sw.stop;
	writefln("Total test duration: %s", sw.peek);
	return rc;
}
